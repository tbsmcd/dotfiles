PATH="$(brew --prefix homebrew/php/php55)/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin:${HOME}/bin:${HOME}/.nodebrew/current/bin:/usr/local/share/npm/bin:$PATH"
#PATH="$(brew --prefix homebrew/php/php55)/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin:${HOME}/Scripts:"
export PATH

export ANDROID_HOME=/usr/local/opt/android-sdk

LDFLAGS="-L/opt/local/lib ${LDFLAGS}"
export LDFLAGS

CPPFLAGS="-I/opt/local/include ${CPPFLAGS}"
export CPPFLAGS

export HOMEBREW_CASK_OPTS="--appdir=/Applications"

MANPATH="/opt/local/share/man:$MANPATH"
export MANPATH

export DISPLAY=:0.0

export LANG='ja_JP.UTF-8'

#export EDITOR='mate_wait'
export HOMEBREW_NO_ANALYTICS=1

alias cp="cp -i"
alias mv="mv -i"
alias ls="ls -v"
alias la="ls -a"
alias ll="ls -lv"
alias l="ls -F"
alias sudo='sudo'
alias vi='vim'
#alias mkps='/Users/tbsmcd/Scripts/Mkps/mkps.php'
#launchctl
#alias phpfpm='/Users/tbsmcd/Scripts/Launchctl/phpfpm.sh'

#mac app
alias gvim='open -a Macvim'

#git
alias gst='git status'

#git flow
alias gitfs='git flow feature start'
alias gitff='git flow feature finish'
alias gitrs='git flow release start'
alias gitrf='git flow release finish'
alias giths='git flow hotfix start'
alias githf='git flow hotfix finish'

alias composer='php ~/composer.phar'

#alias git-submodule-update-all="git submodule foreach 'git checkout master; git pull' "
#lolipop
alias ssh-lolipop='ssh lolipop.jp-2464e5e31c69b90a@ssh047.lolipop.jp -p 2222'
alias ssh-oklab='ssh lolipop.jp-ok-lab@ssh489.lolipop.jp -p 2222'
alias ssh-gw='ssh lolipop.jp-g-wear@ssh482.lolipop.jp -p 2222'
alias ssh-sakura='ssh tbsmcd@49.212.186.55 -p 54302'
alias ssh-heart='ssh heart@heartorg-macmini.1334604318.members.btmm.icloud.com'
alias mosh-heart='mosh --server="/usr/local/bin/mosh-server" -6 heart@heartorg-macmini.1334604318.members.btmm.icloud.com'
alias ssh-pci='ssh -i ~/.ssh/PCI_DIC.pem ubuntu@ec2-52-193-32-49.ap-northeast-1.compute.amazonaws.com'

## 履歴の保存先
HISTFILE=$HOME/.zsh-history
## メモリに展開する履歴の数
HISTSIZE=100000
## 保存する履歴の数
SAVEHIST=100000

## 補完機能の強化
autoload -Uz compinit
compinit -u
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*' group-name ''

## コアダンプサイズを制限
limit coredumpsize 102400
## 出力の文字列末尾に改行コードが無い場合でも表示
unsetopt promptcr

## 色を使う
setopt prompt_subst
## ビープを鳴らさない
setopt nobeep
## 内部コマンド jobs の出力をデフォルトで jobs -l にする
setopt long_list_jobs
## 補完候補一覧でファイルの種別をマーク表示
setopt list_types
## サスペンド中のプロセスと同じコマンド名を実行した場合はリジューム
setopt auto_resume
## 補完候補を一覧表示
setopt auto_list
## 直前と同じコマンドをヒストリに追加しない
setopt hist_ignore_dups
## cd 時に自動で push
setopt auto_pushd
## 同じディレクトリを pushd しない
setopt pushd_ignore_dups
## ファイル名で #, ~, ^ の 3 文字を正規表現として扱う
setopt extended_glob
## TAB で順に補完候補を切り替える
setopt auto_menu
## zsh の開始, 終了時刻をヒストリファイルに書き込む
setopt extended_history
## =command を command のパス名に展開する
setopt equals
## --prefix=/usr などの = 以降も補完
setopt magic_equal_subst
## ヒストリを呼び出してから実行する間に一旦編集
setopt hist_verify
## ファイル名の展開で辞書順ではなく数値的にソート
setopt numeric_glob_sort
## 出力時8ビットを通す
setopt print_eight_bit
## ヒストリを共有
setopt share_history
## 補完候補のカーソル選択を有効に
zstyle ':completion:*:default' menu select=1
## 補完候補の色づけ
#eval `dircolors`
#export ZLS_COLORS=$LS_COLORS
#zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
## ディレクトリ名だけで cd
setopt auto_cd
## カッコの対応などを自動的に補完
setopt auto_param_keys
## ディレクトリ名の補完で末尾の / を自動的に付加し、次の補完に備える
setopt auto_param_slash
## スペルチェック
setopt correct
## {a-c} を a b c に展開する機能を使えるようにする
setopt brace_ccl
## Ctrl+S/Ctrl+Q によるフロー制御を使わないようにする
setopt NO_flow_control
## コマンドラインの先頭がスペースで始まる場合ヒストリに追加しない
setopt hist_ignore_space
## コマンドラインでも # 以降をコメントと見なす
setopt interactive_comments
## ファイル名の展開でディレクトリにマッチした場合末尾に / を付加する
setopt mark_dirs
## history (fc -l) コマンドをヒストリリストから取り除く。
setopt hist_no_store
## 補完候補を詰めて表示
setopt list_packed
## 最後のスラッシュを自動的に削除しない
setopt noautoremoveslash
## プロンプトの設定
autoload colors
colors
PROMPT="%{${fg[blue]}%}[%n@%m] %(!.#.$) %{${reset_color}%}"
PROMPT2="%{${fg[blue]}%}%_> %{${reset_color}%}"
SPROMPT="%{${fg[red]}%}correct: %R -> %r [nyae]? %{${reset_color}%}"
#RPROMPT="%{${fg[blue]}%}[%~]%{${reset_color}%}"

autoload -Uz vcs_info
zstyle ':vcs_info:*' formats '(%s)-[%b]'
zstyle ':vcs_info:*' actionformats '(%s)-[%b|%a]'
precmd () {
	    psvar=()
		    LANG=en_US.UTF-8 vcs_info
			    [[ -n "$vcs_info_msg_0_" ]] && psvar[1]="$vcs_info_msg_0_"
}
RPROMPT="%1(v|%F{green}%1v%f|)%{${fg[blue]}%}[%~]%{${reset_color}%}"

bindkey -v
bindkey "^?" backward-delete-char
setopt nolistbeep

# peco
function peco-snippets() {
	BUFFER=$(grep -v "^#" ~/.snippets | peco --query "$LBUFFER")
	zle clear-screen
}
zle -N peco-snippets
bindkey '^x^s' peco-snippets

