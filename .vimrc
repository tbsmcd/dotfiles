"Show
set showtabline=2  "タブを常に表示
set number "行番号の表示
set showmode "モードの表示
set title "ファイル名の表示
set ruler "ルーラーの表示
set showcmd "編集中コマンドの表示
set showmatch "括弧入力時に対応する括弧の表示
set laststatus=2 "ステータスラインを常時表示
set ignorecase "検索時に大文字小文字を区別しない
set smartcase "大文字 > 大文字, 小文字 > 大文字or小文字で検索
set incsearch "インクリメンタルサーチ
set nowrap "折り返し表示しない
set cursorline "カーソル行ハイライト
set nobackup "バックアップ（~）無し
set swapfile "swapファイルあり
set noundofile

" カッコを自動で閉じる
"inoremap { {}<LEFT>
inoremap {<Enter> {}<Left><CR><ESC>vd<Esc><S-o>
inoremap [<Enter> []<Left><CR><ESC>vd<Esc><S-o>
inoremap [ []<LEFT>
inoremap ( ()<LEFT>
inoremap " ""<LEFT>
inoremap ' ''<LEFT>
vnoremap { "zdi{<C-R>z}<ESC>
vnoremap [ "zdi[<C-R>z]<ESC>
vnoremap ( "zdi(<C-R>z)<ESC>
vnoremap " "zdi"<C-R>z"<ESC>
vnoremap ' "zdi'<C-R>z'<ESC>"'")"]"}'")]}"

"Programing

colorscheme jellybeans
syntax on "カラー表示
set smartindent "オートインデント
set shiftwidth=4 "インデント幅を4に
"tab
set tabstop=4 "tab幅を4に
au BufNewFile,Bufread *.js set tabstop=4 shiftwidth=4 expandtab

"Search

set ignorecase "検索文字列が小文字の場合は大文字小文字を区別なく
set smartcase "検索文字列に大文字が含まれている場合は区別
set wrapscan "検索時に最後まで行ったら最初に戻る
set noincsearch "検索文字列入力時に順次対象文字列にヒットさせない


"PHP

func PhpLint()
	let rslt = system('php -l '.bufname(''))
	if !strlen(matchstr(rslt, '^No syntax errors detected in .*$'))
		echo rslt
	endif
 endfunc
au BufWritePost *.php :call PhpLint()
au BufWritePost *.ctp :call PhpLint()

autocmd BufWritePre * :%s/\s\+$//ge

autocmd FileType php,ctp :set dictionary+=~/.vim/dict/php_functions.dict
set complete+=k

"ビープ
set visualbell t_vb=

"pathogen
call pathogen#runtime_append_all_bundles()

"alias
cmap NTR NERDTree

"esc to eisu
if has('mac')
	let eisuu_cmd = "osascript -e 'tell application \"System Events\" to key code 102' &"
	autocmd InsertLeave * call system(eisuu_cmd)
	noremap <silent> <Esc> <Esc>:call system(eisuu_cmd)<CR>
endif
