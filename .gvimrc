:colorscheme jellybeans
" タブ
map <silent> gw :macaction selectNextWindow:
map <silent> gW :macaction selectPreviousWindow:

" Font
set guifont=Source\ Code\ Pro:h14

" ime
set imdisable

" Window
set columns=150
set lines=49
set transparency=12
set guioptions-=T "ツールバー非表示

" snipMate用設定
autocmd FileType php set ft=php.php_cakephp_bundle
autocmd FileType ctp set ft=php.html.php_cakephp_bundle

